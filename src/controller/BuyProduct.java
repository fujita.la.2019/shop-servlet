package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.beans.Product;
import model.beans.User;
import model.dao.DAOException;
import model.dao.HistoryDAO;
import model.dao.ProductDAO;
import model.dao.StockDAO;

/**
 * Servlet implementation class BuyProduct
 */
@WebServlet("/buy")
public class BuyProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyProduct() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code = request.getParameter("code");

		try {
			ProductDAO productDAO = new ProductDAO();
			Product product = productDAO.selectByCode(code);
			//在庫チェック
			if(product.getStock() < 1) {
				//在庫切れ
				return;
			}

			StockDAO stockDao = new StockDAO();
			stockDao.decrementStockByCode(code);

			User user = (User)request.getSession().getAttribute("user");
			HistoryDAO historyDao = new HistoryDAO();
			historyDao.insertHistory(user.getId(), code, 1);

			response.sendRedirect("/shop-servlet/history");


		} catch (DAOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
