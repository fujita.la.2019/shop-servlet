package model.beans;

import java.sql.Timestamp;

public class History {
	/**
	 * 履歴ID
	 */
	private int historyId;

	/**
	 * 購入者ID
	 */
	private String userId;

	/**
	 * 購入者名
	 */
	private String userName;

	/**
	 * 製品コード
	 */
	private String productCode;

	/**
	 * 製品名
	 */
	private String productName;

	/**
	 * 単価
	 */
	private int productPrice;
	/**
	 * 購入数
	 */
	private int productSaleCount;

	/**
	 * 購入日
	 */
	private Timestamp timestamp;

	/**
	 * コンストラクタ
	 * @param historyId
	 * @param userId
	 * @param userName
	 * @param productCode
	 * @param productName
	 * @param productPrice
	 * @param productSaleCount
	 * @param timestamp
	 */
	public History(int historyId, String userId, String userName, String productCode, String productName,
			int productPrice, int productSaleCount, Timestamp timestamp) {
		super();
		this.historyId = historyId;
		this.userId = userId;
		this.userName = userName;
		this.productCode = productCode;
		this.productName = productName;
		this.productPrice = productPrice;
		this.productSaleCount = productSaleCount;
		this.timestamp = timestamp;
	}

	/**
	 * @return historyId
	 */
	public int getHistoryId() {
		return historyId;
	}

	/**
	 * @param historyId セットする historyId
	 */
	public void setHistoryId(int historyId) {
		this.historyId = historyId;
	}

	/**
	 * @return userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId セットする userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName セットする userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return productCode
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * @param productCode セットする productCode
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * @return productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName セットする productName
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return productPrice
	 */
	public int getProductPrice() {
		return productPrice;
	}

	/**
	 * @param productPrice セットする productPrice
	 */
	public void setProductPrice(int productPrice) {
		this.productPrice = productPrice;
	}

	/**
	 * @return productSaleCount
	 */
	public int getProductSaleCount() {
		return productSaleCount;
	}

	/**
	 * @param productSaleCount セットする productSaleCount
	 */
	public void setProductSaleCount(int productSaleCount) {
		this.productSaleCount = productSaleCount;
	}

	/**
	 * @return timestamp
	 */
	public Timestamp getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp セットする timestamp
	 */
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

}
