package model.beans;

import java.io.Serializable;

public class Stock implements Serializable {
	/**
	 * 商品コード
	 */
	private String code;

	/**
	 * ストック数
	 */
	private int stockcount;

	/**
	 * コンストラクタ
	 * @param code
	 * @param stockcount
	 */
	public Stock(String code, int stockcount) {
		this.code = code;
		this.stockcount = stockcount;
	}

	/**
	 * @return code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code セットする code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return stockcount
	 */
	public int getStockcount() {
		return stockcount;
	}

	/**
	 * @param stockcount セットする stockcount
	 */
	public void setStockcount(int stockcount) {
		this.stockcount = stockcount;
	}
}
