package model.beans;

import java.io.Serializable;

public class User implements Serializable {
	/**
	 * ユーザーID
	 */
	private String id;

	/**
	 * パスワード
	 */
	private String password;

	/**
	 * 名前
	 */
	private String name;

	/**
	 * コンストラクタ
	 * @param id ユーザID
	 * @param password パスワード
	 * @param name 表示名
	 */
	public User(String id, String password, String name) {
		this.id = id;
		this.password = password;
		this.name = name;
	}

	/**
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password セットする password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name セットする name
	 */
	public void setName(String name) {
		this.name = name;
	}
}
