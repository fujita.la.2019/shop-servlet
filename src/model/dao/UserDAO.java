package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.beans.User;

public class UserDAO extends BaseDAO<User> {
	/**
	 * テーブル名
	 */
	private final String TABLE = "users";

	public UserDAO() throws DAOException {
		super();
	}

	public User select(String userId, String password) throws DAOException {
		if(this.getCon() == null) {
			this.getConection();
		}

		final String SQL = "SELECT * FROM " + this.TABLE + " WHERE password = crypt(?, password) and id = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = this.getCon().prepareStatement(SQL);
			st.setString(1, password);
			st.setString(2, userId);
			rs = st.executeQuery();

			if(rs.next()) {
				String id = rs.getString("id");
				String name = rs.getString("name");
				String hashedPassword = rs.getString("password");
				return new User(id, hashedPassword, name);
			}else {
				return null;
			}

		}catch(Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました");
		}finally {
			try {
				if(rs != null) {
					rs.close();
				}
				if(st != null) {
					st.close();
				}
				this.close();
			}catch (Exception e) {
				e.printStackTrace();
				throw new DAOException("リソースの開放に失敗しました");
			}
		}
	}

	public boolean isCorrectUser(String userId, String password) throws DAOException {
		if(this.getCon() == null) {
			this.getConection();
		}

		final String SQL = "SELECT password = crypt(?, password) FROM " + this.TABLE + " WHERE id = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = this.getCon().prepareStatement(SQL);
			st.setString(1, password);
			st.setString(2, userId);
			rs = st.executeQuery();

			rs.next();
			if(rs.getRow() != 0) {
				return rs.getBoolean(1);
			}else {
				return false;
			}
		}catch(Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました");
		}finally {
			try {
				if(rs != null) {
					rs.close();
				}
				if(st != null) {
					st.close();
				}
				this.close();
			}catch (Exception e) {
				e.printStackTrace();
				throw new DAOException("リソースの開放に失敗しました");
			}
		}
	}

	public List<User> selectAll() throws DAOException {
		if(this.getCon() == null) {
			this.getConection();
		}

		final String SQL = "SELECT * FROM " + this.TABLE;
		try(PreparedStatement st = this.getCon().prepareStatement(SQL); ResultSet rs = st.executeQuery()) {
			List<User> result = new ArrayList<>();
			while(rs.next()) {
				String id = rs.getString(1);
				String password = rs.getString(2);
				String name = rs.getString(3);
				User user = new User(id, password, name);
				result.add(user);
			}
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました");
		}finally {
			try {
				this.getCon().close();
			}catch (Exception e) {
				e.printStackTrace();
				throw new DAOException("リソースの開放に失敗しました");
			}
		}
	}
}
