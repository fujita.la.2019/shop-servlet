package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.beans.Product;

public class ProductDAO extends BaseDAO<Product> {

	/**
	 * コンストラクタ
	 * @throws DAOException
	 */
	public ProductDAO() throws DAOException {
		super();
	}

	/**
	 * 製品コードから一意の製品情報を取得する
	 * @param keyword
	 * @return
	 * @throws DAOException
	 */
	public Product selectByCode(String keyword) throws DAOException {
		if(this.getCon() == null) {
			this.getConection();
		}

		final String SQL = "SELECT products.code, products.name, products.price, stocks.stockcount FROM products INNER JOIN stocks ON products.code = stocks.code WHERE products.code = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = this.getCon().prepareStatement(SQL);
			st.setString(1, keyword);
			rs = st.executeQuery();

			rs.next();
			String code = rs.getString("code");
			String name = rs.getString("name");
			int price = rs.getInt("price");
			int stock = rs.getInt("stockcount");

			return new Product(code, name, price, stock);

		}catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました");
		}finally {
			try {
				if(rs != null) {
					rs.close();
				}
				if(st != null) {
					st.close();
				}
				this.getCon().close();
			}catch (Exception e) {
				e.printStackTrace();
				throw new DAOException("リソースの開放に失敗しました");
			}

		}
	}

	@Override
	public List<Product> selectAll() throws DAOException {
		if(this.getCon() == null) {
			this.getConection();
		}

		final String SQL = "SELECT products.code, products.name, products.price, stocks.stockcount FROM products INNER JOIN stocks ON products.code = stocks.code";
		try(PreparedStatement st = this.getCon().prepareStatement(SQL); ResultSet rs = st.executeQuery()) {
			List<Product> result = new ArrayList<>();
			while(rs.next()) {
				String code = rs.getString("code");
				String name = rs.getString("name");
				int price = rs.getInt("price");
				int stock = rs.getInt("stockcount");
				Product product = new Product(code, name, price, stock);
				result.add(product);
			}

			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました");
		}finally {
			try {
				this.getCon().close();
			}catch (Exception e) {
				e.printStackTrace();
				throw new DAOException("リソースの開放に失敗しました");
			}
		}
	}

}
