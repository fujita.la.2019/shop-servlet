package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.beans.Stock;

public class StockDAO extends BaseDAO<Stock> {

	/**
	 * コンストラクタ
	 * @throws DAOException
	 */
	public StockDAO() throws DAOException {
		super();
	}

	/**
	 * 製品コード指定で財誤情報を取得
	 * @param code
	 * @return
	 * @throws DAOException
	 */
	public Stock select(String code) throws DAOException {
		if(this.getCon() == null) {
			this.getConection();
		}

		final String SQL = "SELECT * FROM stocks WHERE code = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = this.getCon().prepareStatement(SQL);
			st.setString(1, code);
			rs = st.executeQuery();

			if(rs.next()) {
				String stockCode = rs.getString("code");
				int stockCount = rs.getInt("stockcount");
				return new Stock(stockCode, stockCount);
			}else {
				return null;
			}
		}catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました");
		}finally {
			try {
				if(rs != null) {
					rs.close();
				}
				if(st != null) {
					st.close();
				}
				this.getCon().close();
			}catch (Exception e) {
				e.printStackTrace();
				throw new DAOException("リソースの開放に失敗しました");
			}
		}
	}

	@Override
	public List<Stock> selectAll() throws DAOException {
		if(this.getCon() == null) {
			this.getConection();
		}

		final String SQL = "SELECT * FROM stocks";
		try(PreparedStatement st = this.getCon().prepareStatement(SQL); ResultSet rs = st.executeQuery()) {
			List<Stock> result = new ArrayList<>();
			while(rs.next()) {
				String code = rs.getString("code");
				int stockcount = rs.getInt("stockcount");
				Stock stock = new Stock(code, stockcount);
				result.add(stock);
			}
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました");
		}finally {
			try {
				this.getCon().close();
			}catch (Exception e) {
				e.printStackTrace();
				throw new DAOException("リソースの開放に失敗しました");
			}
		}
	}

	public void decrementStockByCode(String code) throws DAOException {
		if(this.getCon() == null) {
			this.getConection();
		}


		final String SQL = "UPDATE stocks SET stockcount = stockcount - 1 WHERE code = ?";
		PreparedStatement st = null;
		try {
			//トランザクション処理のためにautoCommitをfalseに設定
			this.getCon().setAutoCommit(false);
			st = this.getCon().prepareStatement(SQL);
			st.setString(1, code);
			st.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました");
		}finally {
			try {
				this.getCon().commit();
				if(st != null) {
					st.close();
				}
				this.getCon().close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new DAOException("リソースの開放に失敗しました");
			}
		}

	}

}
