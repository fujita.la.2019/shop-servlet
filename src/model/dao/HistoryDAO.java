package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.beans.History;

public class HistoryDAO extends BaseDAO<History>{

	/**
	 * コンストラクタ
	 * @throws DAOException
	 */
	public HistoryDAO() throws DAOException {
		super();
	}

	public void insertHistory(String userId, String code, int salecount) throws DAOException {
		if(this.getCon() == null) {
			this.getConection();
		}

		final String SQL ="INSERT INTO historys (user_id, code, salecount, saledate) VALUES (?, ?, ?, ?)";
		PreparedStatement st = null;
		try {
			this.getCon().setAutoCommit(false);
			Timestamp timestamp = new Timestamp(new Date().getTime());
			st = this.getCon().prepareStatement(SQL);
			st.setString(1, userId);
			st.setString(2, code);
			st.setInt(3, salecount);
			st.setTimestamp(4, timestamp);
			st.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました");
		}finally {
			try {
				this.getCon().commit();
				if(st != null) {
					st.close();
				}
				this.getCon().close();
			} catch (Exception e) {
				e.printStackTrace();
				throw new DAOException("リソースの開放に失敗しました");
			}
		}
	}

	public List<History> selectByUserId(String id) throws DAOException {
		if(this.getCon() == null) {
			this.getConection();
		}

		final String SQL ="SELECT historys.id AS historyId, users.id AS userId, users.name AS userName, products.code AS productCode, products.name AS productName, products.price AS productPrice, historys.salecount AS productSalecount, historys.saledate AS timestamp FROM historys INNER JOIN users ON historys.user_id = users.id INNER JOIN products ON historys.code = products.code WHERE users.id = ? ORDER BY historys.saledate DESC;";
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = this.getCon().prepareStatement(SQL);
			st.setString(1, id);
			rs = st.executeQuery();
			List<History> result = new ArrayList<>();
			while(rs.next()) {
				int historyId = rs.getInt(1);
				String userId = rs.getString(2);
				String userName = rs.getString(3);
				String productCode = rs.getString(4);
				String productName = rs.getString(5);
				int productPrice = rs.getInt(6);
				int saleCount = rs.getInt(7);
				Timestamp timestamp = rs.getTimestamp(8);
				History history = new History(historyId, userId, userName, productCode, productName, productPrice, saleCount, timestamp);
				result.add(history);
			}
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました");
		}finally {
			try {
				if(rs != null) {
					rs.close();
				}
				if(st != null) {
					st.close();
				}
				this.getCon().close();
			} catch (Exception e) {
				e.printStackTrace();
				throw new DAOException("リソースの開放に失敗しました");
			}

		}
	}

	@Override
	public List<History> selectAll() throws DAOException {
		if(this.getCon() == null) {
			this.getConection();
		}

		final String SQL ="SELECT historys.id AS historyId, users.id AS userId, users.name AS userName, products.code AS productCode, products.name AS productName product.price AS productPrice, historys.salecount AS productSalecount, historys.saledate AS timestamp FROM historys INNER JOIN users ON historys.user_id = users.id INNER JOIN products ON historys.code = products.code ORDER BY historys.saledate DESC;";
		try(PreparedStatement st = this.getCon().prepareStatement(SQL); ResultSet rs = st.executeQuery()) {
			List<History> result = new ArrayList<>();
			while(rs.next()) {
				int historyId = rs.getInt(1);
				String userId = rs.getString(2);
				String userName = rs.getString(3);
				String productCode = rs.getString(4);
				String productName = rs.getString(5);
				int productPrice = rs.getInt(6);
				int saleCount = rs.getInt(7);
				Timestamp timestamp = rs.getTimestamp(8);
				History history = new History(historyId, userId, userName, productCode, productName, productPrice, saleCount, timestamp);
				result.add(history);
			}
			return result;
		}catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました");
		}finally {
			try {
				this.getCon().close();
			} catch (Exception e) {
				e.printStackTrace();
				throw new DAOException("リソースの開放に失敗しました");
			}

		}
	}

}
