<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="../layout/layout.jsp">
	<jsp:param name="title" value="製品一覧" />
	<jsp:param name="content">
		<jsp:attribute name="value">
<h1>${ param.title }</h1>
<table class="centered highlight">
	<tr>
		<th>商品コード</th>
		<th>商品名</th>
		<th>価格</th>
		<th>在庫数</th>
		<th>購入</th>
	</tr>
	<c:forEach var="product" items="${ products }">
		<tr>
			<td>${ product.code }</td>
			<td>${ product.name }</td>
			<td>${ product.price }円</td>
			<td>${ product.stock }個</td>
			<td>
			<a href="/shop-servlet/buy?code=${ product.code }" class="btn waves-effect waves-light <c:if test="${ product.stock == 0 }">
				<c:out value="disabled"/>
			</c:if>">購入</a>
			</td>
		</tr>
	</c:forEach>
</table>
		</jsp:attribute>
	</jsp:param>
</jsp:include>