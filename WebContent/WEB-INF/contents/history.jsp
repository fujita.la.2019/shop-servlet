<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="../layout/layout.jsp">
	<jsp:param name="title" value="購入履歴" />
	<jsp:param name="content">
		<jsp:attribute name="value">
<h1>${ param.title }</h1>
<table class="centered highlight">
<thead>
	<tr>
		<th>購入日</th>
		<th>購入コード</th>
		<th>商品名</th>
		<th>購入数</th>
		<th>単価</th>
		<th>金額</th>
	</tr>
</thead>
<tbody>
	<c:forEach var="history" items="${ historys }">
		<tr>
			<td>${ history.timestamp }</td>
			<td>${ history.productCode }</td>
			<td>${ history.productName }</td>
			<td>${ history.productSaleCount }個</td>
			<td>${ history.productPrice }円</td>
			<td>${ history.productPrice * history.productSaleCount }</td>
		</tr>
	</c:forEach>
</tbody>
</table>
		</jsp:attribute>
	</jsp:param>
</jsp:include>