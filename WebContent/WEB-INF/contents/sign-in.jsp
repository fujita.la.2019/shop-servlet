<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page session="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="../layout/layout.jsp">
	<jsp:param name="title" value="サインイン" />
	<jsp:param name="content">
		<jsp:attribute name="value">
<h1>${ param.title }</h1>
<c:if test="${not empty errorMsg }">
<p class="red-text">${ errorMsg }</p>
</c:if>
<div class="card-panel">
<form action="/shop-servlet/sign-in-result" method="post">
	<div class="row">
		<div class="input-field col s12">
			<i class="material-icons prefix">account_circle</i> <input
								id="user-id" type="text" class="validate" name="user-id" value="${ userId }"
								required> <label for="user-id">アカウントID</label>
		</div>
		<div class="input-field col s12">
			<i class="material-icons prefix">lock</i> <input id="password"
								type="password" class="validate" name="password" required> <label
								for="password">パスワード</label>
		</div>
		<div class="col s12">
			<button class="btn waves-effect waves-light" type="submit"
								name="action">ログイン
   				 <i class="material-icons right">send</i>
  			</button>
 		 </div>
	</div>
</form>
</div>
		</jsp:attribute>
	</jsp:param>
</jsp:include>