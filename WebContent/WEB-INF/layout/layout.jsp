<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ショップ | ${param.title}</title>

<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
</head>
<body>
	<nav>
		<div class="nav-wrapper">
			<a href="#!" class="brand-logo center">ショップ</a> <a href="#"
				data-target="mobile-demo" class="sidenav-trigger"><i
				class="material-icons">menu</i></a>
			<ul class="right hide-on-med-and-down">
				<c:if test="${ empty user }">
					<li><a href="/shop-servlet/sign-in">サインイン</a></li>
				</c:if>
				<c:if test="${ not empty user }">
					<li><a href="/shop-servlet/products">商品一覧</a></li>
					<li><a href="/shop-servlet/sign-out">サインアウト</a></li>
				</c:if>
			</ul>
		</div>
	</nav>

	<ul class="sidenav" id="mobile-demo">
		<c:if test="${ empty user }">
			<li><a href="/shop-servlet/sign-in">商品一覧</a></li>
		</c:if>
		<c:if test="${ not empty user }">
			<li><a href="/shop-servlet/products">商品一覧</a></li>
			<li><a href="/shop-servlet/sign-out">サインアウト</a></li>
		</c:if>
	</ul>
	<div class="row">
		<div>
			<div class="col s12 m10 offset-m1 l8 offset-l2">${param.content}</div>
		</div>
	</div>
	<script>
  	M.AutoInit();
  </script>
</body>
</html>